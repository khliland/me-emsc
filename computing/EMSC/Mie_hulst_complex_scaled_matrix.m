function [Q] = Mie_hulst_complex_scaled_matrix(RefIndexComplex, wn, gammaCoef, alphaCoef)
% Calculates approximate extinction of electromagnetic radiation by a spehere.
%
%  ---------------------------------------------------------------------------------
%  Written by:
%  Johanne Solheim, Evgeniy Gunko, Tatiana Konevskikh, Achim Kohler
%
%  Faculty of Science and Technology (REALTEK)
%  Norwegian Unversity of Life Sciences (www.nmbu.no)
%
%  Post address:
%  PO Box 5003, 1432 Aas, Norway
%
%  ---------------------------------------------------------------------------------
%
%  Input:
%  RefIndexComplex  - Imaginary and real fluctuating part of refractive index (row vector)
%  wn               - Wavenumbers corresponding to RefIndexComplex (coloumn vector)
%  gammaCoef        - Physical paramter gamma (float)
%  alphaCoef        - Physical paramter alpha0  (float)
%
%  Output:
%  Q    - Mie extinction curve (row vector)

nv = real(RefIndexComplex);
nvp = imag(RefIndexComplex);

nA = length(alphaCoef);
nG = length(gammaCoef);
divider = ( 1.0 ./ gammaCoef' ) + nv;
tanbeta = nvp ./ divider;
beta0 = atan2( nvp, divider );
cosB  = cos(beta0);
cos2B = cos(2.*beta0);
rhovc = ( 1.0 + gammaCoef' .* nv ) .* (wn'.*100); % 100 is correcting for the unit cm^-1

Q = zeros(nA*nG,length(wn),'single');
for i=1:nA
    rhov = alphaCoef(i) .* rhovc;  

    rhovcosB = cosB./rhov;
    Q(i:nA:end,:) = 2.0 + ( 4 .* rhovcosB ) .* ( - exp( -(rhov) .* (tanbeta) ) .* ( sin( (rhov) - (beta0) ) ...
        + cos((rhov-2.*beta0)).*rhovcosB )...
        + cos2B.*rhovcosB );
%         Q(i:nA:end,:) = 2.0 + ( 4 ./ ( rhov ) ) .* ( cosB ) .* ( - exp( -(rhov) .* (tanbeta) ) .* ( sin( (rhov) - (beta0) ) ...
%             + ( 1.0 ./ ( rhov ) ) .* (cosB) .* cos( ( rhov ) - 2 .* (beta0) ) )...
%             + ( 1.0 ./ ( rhov ) ) .* (cosB) .* cos( 2 .* (beta0) ) );
end

% % ------- Loopless takes more time because of repelem and kron
% tanbeta = repelem(nvp ./ divider,nA,1);
% beta0 = repelem(atan2( nvp, divider ),nA,1);
% cosB  = cos(beta0);
% cos2B = cos(2.*beta0);
% rhov  = kron(( 1.0 + gammaCoef' .* nv ) .* (wn'.*100),alphaCoef'); % 100 is correcting for the unit cm^-1
% rhovcosB = cosB./rhov;
% Q = 2.0 + ( 4 .* rhovcosB ) .* ( - exp( -(rhov) .* (tanbeta) ) .* ( sin( (rhov) - (beta0) ) ...
%         + cos((rhov-2.*beta0)).*rhovcosB )...
%         + cos2B.*rhovcosB );
% % -------